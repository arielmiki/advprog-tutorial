package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        if (salary < 100000.00) {
            throw new IllegalArgumentException("CTO Salary must not lower than 200000.00");
        }
        super.name = name;
        super.salary = salary;
        super.role = "CTO";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
