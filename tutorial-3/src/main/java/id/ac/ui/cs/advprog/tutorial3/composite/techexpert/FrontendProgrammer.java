package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {

    public FrontendProgrammer(String name, double salary) {
        if (salary < 30000.00) {
            throw new IllegalArgumentException(
                    "Frontend Programmer Salary must not lower than 30000.00");
        }
        super.name = name;
        super.salary = salary;
        super.role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
