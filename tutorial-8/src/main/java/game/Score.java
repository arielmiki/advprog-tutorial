package game;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class Score {
    private static AtomicLong score = new AtomicLong(
            Double.doubleToLongBits(100));
    private static AtomicInteger time = new AtomicInteger(0);

    public static void decrement() {
        score.updateAndGet(
            n -> Double.doubleToLongBits(
                        Double.longBitsToDouble(n) - 1));
        time.incrementAndGet();
    }

    public static void addScore(double percentage) {
        score.updateAndGet(
            n -> Double.doubleToLongBits(
                        Double.longBitsToDouble(n) * (1 + percentage)));
    }

    public static void reset() {
        score.getAndSet(Double.doubleToLongBits(100));
        time.getAndSet(0);
    }

    public static double get() {
        return Double.longBitsToDouble(score.get());
    }

    public static int getTime() {
        return time.get();
    }


}
