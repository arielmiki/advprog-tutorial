package sorting;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SortAndSearchTest {
    private int[] testInput;
    private final int[] expected = new int[]{1, 2, 3, 4, 5};

    @Before
    public void setUp() {
        testInput = new int[]{2, 5, 1, 3, 4};
    }


    @Test
    public void testSlowSortAlgorithm() {
        int[] result = Sorter.slowSort(testInput);
        assertArrayEquals(result, expected);
    }

    @Test
    public void testFastSortAlgorithm() {
        int[] result = Sorter.fastSort(testInput);
        assertArrayEquals(result, expected);
    }

    @Test
    public void testSlowSearchAlgorithm() {
        int result = Finder.slowSearch(testInput, 3);
        assertEquals(3, result);
    }

    @Test
    public void testFastSearchAlgorithm() {
        int result = Finder.fastSearch(testInput, 3);
        assertEquals(3, result);
    }

}
