import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


public class CustomerTest {

    private Customer customer;
    private Movie movie;
    private Rental rent;
    private Rental newReleaseRent;
    private Rental childrenRent;

    @Before
    public void setUp() {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
        newReleaseRent = new Rental(new Movie("!", Movie.NEW_RELEASE), 3);
        childrenRent = new Rental(new Movie("Pororo", Movie.CHILDREN), 4);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {

        customer.addRental(rent);
        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(newReleaseRent);
        customer.addRental(childrenRent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 15.5"));
        assertTrue(result.contains("4 frequent renter points"));

    }

    @Test
    public void htmlStatementWithSingleMovies() {
        customer.addRental(rent);
        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));

    }

    @Test
    public void htmlStatementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(newReleaseRent);
        customer.addRental(childrenRent);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");


        assertEquals(8, lines.length);
        assertTrue(result.contains("Amount owed is 15.5"));
        assertTrue(result.contains("4 frequent renter points"));

    }
}