import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        String result = "Rental Record for " + getName() + "\n";
        for (Rental rental : rentals) {

            result += "\t" + rental.getMovie().getTitle() + "\t"
                    + String.valueOf(rental.getAmount()) + "\n";

        }
        result += "Amount owed is " + String.valueOf(getTotalAmount()) + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoints())
                + " frequent renter points";

        return result;
    }

    public String htmlStatement() {
        String result = "<h1> Rental Record for" + getName() + "</h1>\n";
        result += "<ul>\n";
        for (Rental rental : rentals) {
            result += "<li><pre>\t" + rental.getMovie().getTitle() + "\t"
                    + String.valueOf(rental.getAmount()) + "Z </pre></li>\n";
        }
        result += "</ul>\n";
        result += "<h3> Amount owed is " + String.valueOf(getTotalAmount()) + " </h3>\n";
        result += "<h3> You earned " + String.valueOf(getTotalFrequentRenterPoints())
                + " frequent renter points</h3>";

        return result;
    }

    private double getTotalAmount() {
        double result = 0;
        for (Rental rental : rentals) {
            result += rental.getAmount();
        }
        return result;
    }

    private int getTotalFrequentRenterPoints() {
        int result = 0;
        for (Rental rental : rentals) {
            result += rental.getFrequentRenterPoints();
        }
        return result;
    }


}