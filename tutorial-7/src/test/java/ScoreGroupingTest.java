import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ScoreGroupingTest {
    private static final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private static final ByteArrayOutputStream err = new ByteArrayOutputStream();

    private static final List<String> NAME =
            Arrays.asList("Andy", "Annie", "Choco", "Gophers");
    private static Map<String, Integer> map;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
        map = NAME.stream().collect(Collectors
                .toMap(Function.identity(), String::length));
    }

    @Test
    public void testScoreGroupingIsCorrect() {
        assertEquals(2,
                ScoreGrouping.groupByScores(map).get(5).size());
        assertNotEquals(2,
                ScoreGrouping.groupByScores(map).get(4).size());

        assertTrue(ScoreGrouping.groupByScores(map)
                .get(5).contains("Annie"));
        assertFalse(ScoreGrouping.groupByScores(map)
                .get(4).contains("Annie"));
    }

    @Test
    public void testMainFunction() {
        ScoreGrouping.main(new String[]{"Hello", "World"});
        assertEquals(
                "{11=[Charlie, Foxtrot], 12=[Alice], 15=[Emi, Bob, Delta]}\n",
                out.toString());
    }


    @After
    public void restoreStream() {
        System.setOut(System.out);
        System.setErr(System.err);
    }
}