package applicant;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ApplicantTest {
    private static final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private static final ByteArrayOutputStream err = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(out));
        System.setErr(new PrintStream(err));
    }

    @Test
    public void testInputOutput() {
        Applicant.main(new String[]{"Hello", "World"});
        String expected = "Result of evaluating applicant: accepted\n"
                + "Result of evaluating applicant: accepted\n"
                + "Result of evaluating applicant: rejected\n"
                + "Result of evaluating applicant: rejected\n";
        assertEquals(expected, out.toString());
    }

    @After
    public void restoreStream() {
        System.setOut(System.out);
        System.setErr(System.err);
    }
}
