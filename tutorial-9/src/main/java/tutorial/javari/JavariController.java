package tutorial.javari;

import java.util.Collection;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.AnimalRepository;


@RestController
public class JavariController {
    private static final AnimalRepository model = AnimalRepository.getInstance();
    private static final String OK = "{\"status\":\"ok\"}";
    private static final String FAILED = "{\"status\":\"failed\"}";

    @RequestMapping(value = "/javari", method = RequestMethod.GET)
    public Collection<Animal> getAnimals() {
        return model.findAll();
    }

    @RequestMapping(value = "/javari", method = RequestMethod.POST,
            produces = "application/json")
    public String addAnimal(@RequestBody Animal animal) {
        try {
            int id = animal.getId();
            if (model.findById(id) != null
                    || !model.save(animal)) {
                return FAILED;
            }

        } catch (Exception e) {
            throw new BadRequestException("No ID is Specified");
        }
        return OK;
    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.GET)
    public Animal getAnimal(@PathVariable int id) {
        if (model.findById(id) == null) {
            throw new NotFoundException();
        }
        return model.findById(id);

    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.DELETE,
            produces = "application/json")
    public String deleteAnimal(@PathVariable int id) {
        if (!model.removeById(id)) {
            throw new NotFoundException();
        }
        return OK;
    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.PUT,
            produces = "application/json")
    public Animal updateAnimal(@PathVariable int id, @RequestBody Animal animal) {
        if (model.findById(id) == null) {
            throw new NotFoundException();
        }
        if (!model.save(animal)) {
            throw new BadRequestException("Object is Null");
        }
        return animal;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    class NotFoundException extends RuntimeException {
        NotFoundException() {
            super("Content Not Found");
        }

    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    class BadRequestException extends RuntimeException {
        BadRequestException(String msg) {
            super(msg);
        }
    }

}


