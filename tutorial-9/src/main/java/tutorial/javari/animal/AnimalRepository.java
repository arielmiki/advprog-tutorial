package tutorial.javari.animal;

import java.io.BufferedWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;


public class AnimalRepository {
    private static final AnimalRepository INSTANCE = new AnimalRepository();
    private static final String LOCATION = "db.csv";

    private Map<Integer, Animal> animals;

    private AnimalRepository() {
        animals = new HashMap<>();
        try {
            Reader reader = Files.newBufferedReader(Paths.get(LOCATION));
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                    .withFirstRecordAsHeader()
                    .withIgnoreHeaderCase()
                    .withTrim());
            csvParser.forEach(record -> {
                animals.put(Integer.parseInt(record.get("id")),
                        new Animal(
                                Integer.parseInt(record.get("id")),
                                record.get("type"),
                                record.get("name"),
                                Gender.parseGender(record.get("gender")),
                                Double.parseDouble(record.get("length")),
                                Double.parseDouble(record.get("weight")),
                                Condition.parseCondition(record.get("condition"))
                        ));
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private synchronized void sync() {
        try {
            BufferedWriter writer = Files.newBufferedWriter(Paths.get(LOCATION));
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                    .withHeader("id", "type", "name", "gender", "length", "weight", "condition"));
            for (Animal animal : animals.values()) {
                csvPrinter.printRecord(
                        animal.getId(),
                        animal.getType(),
                        animal.getName(),
                        animal.getGender(),
                        animal.getLength(),
                        animal.getWeight(),
                        animal.getCondition()
                );
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static AnimalRepository getInstance() {
        return INSTANCE;
    }

    public Animal findById(int id) {
        return animals.get(id);
    }

    public boolean removeById(int id) {
        boolean res = animals.remove(id) != null;
        sync();
        return res;
    }

    public boolean save(Animal animal) {
        if (animal == null) {
            return false;
        }
        animals.put(animal.getId(), animal);
        sync();
        return true;
    }

    public Collection<Animal> findAll() {
        return animals.values();
    }

}
