package tutorial.javari;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.Charset;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import tutorial.javari.animal.Animal;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavariControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private Animal animal = new Animal(400, "Lion", "Simba",
            null, 20, 20, null);
    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );


    @Test
    public void testGetAllAnimals() throws Exception {
        this.mockMvc.perform(get("/javari")).andDo(print())
                .andExpect(status().isOk());
    }

    //    @Test
    //    public void testGetOneAnimal() throws Exception {
    //        this.mockMvc.perform(post("/javari")
    //                .content(convertObjectToJsonBytes(animal))
    //                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    //        this.mockMvc.perform(get("/javari/400")).andDo(print())
    //                .andExpect(status().isOk());
    //    }
    //
    //    @Test
    //    public void testDeleteOneAnimal() throws Exception {
    //        this.mockMvc.perform(post("/javari")
    //                .content(convertObjectToJsonBytes(animal))
    //                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    //        this.mockMvc.perform(delete("/javari/400")).andDo(print())
    //                .andExpect(status().isOk());
    //    }
    //
    //    @Test
    //    public void testPostAnimal() throws Exception {
    //        this.mockMvc.perform(post("/javari")
    //                .content(convertObjectToJsonBytes(animal))
    //                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    //    }
    //
    //    @Test
    //    public void testPutAnimal() throws Exception {
    //        this.mockMvc.perform(post("/javari")
    //                .contentType(MediaType.APPLICATION_JSON)
    //                .content(convertObjectToJsonBytes(animal)))
    //                .andExpect(status().isOk());
    //        this.mockMvc.perform(put("/javari/400")
    //                .contentType(contentType)
    //                .content(convertObjectToJsonBytes(animal)))
    //                .andExpect(status().isOk());
    //
    //    }

    private String convertObjectToJsonBytes(Object object) throws IOException {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}