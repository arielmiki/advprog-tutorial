package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class MustardSauce implements Sauce {
    public String toString() {
        return "Mustard Sauce";
    }
}
