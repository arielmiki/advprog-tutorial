package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class FakeClams implements Clams {

    public String toString() {
        return "Fake Clams from San Junipero";
    }
}
